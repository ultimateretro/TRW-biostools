#!/usr/bin/python3 -u
#
# 86Box          A hypervisor and IBM PC system emulator that specializes in
#                running old operating systems and software designed for IBM
#                PC systems and compatibles from 1981 through fairly recent
#                system designs based on the PCI bus.
#
#                This file is part of the 86Box BIOS Tools distribution.
#
#                Main BIOS extractor and analyzer program.
#
#
#
# Authors:       RichardG, <richardg867@gmail.com>
#
#                Copyright 2021 RichardG.
#

import errno, getopt, multiprocessing, os, pickle, re, socket, subprocess, sys, threading, hashlib, json
from biostools import analyzers, extractors, formatters, util
from decouple import config
from datetime import datetime
from aiofile import async_open
import pandas as pd
import discord, shutil, re, openpyxl, requests, asyncio, aiohttp, lxml.html, urllib, queue, os
import requests.exceptions as RequestException
from logging.handlers import QueueHandler
import logging, io
from ast import literal_eval
from multiprocessing import current_process
from multiprocessing import Process
from discord.ext import commands
from shutil import copy

lines = [["File","Vendor","Version","String","Sign-on","Metadata","ROMs"]]
status = 0


# Constants.
ANALYZER_MAX_CACHE_MB = 512
DEFAULT_REMOTE_PORT = 8620
# Extraction module.

def extract_file(file_extractors, subdir_trim_index, path_trim_index, next_dir_number_path, scan_dir_path, scan_file_name):
	"""Process a given file for extraction."""

	# Build source file path.
	file_path = os.path.join(scan_dir_path, scan_file_name)

	# Remove links.
	if os.path.islink(file_path):
		try:
			os.remove(file_path)
		except:
			try:
				os.rmdir(file_path)
			except:
				pass
		return

	# Read header.
	try:
		f = open(file_path, 'rb')
		file_data = f.read(32782) # upper limit set by ISOExtractor
		f.close()
	except:
		# The file might have been removed after the fact by an extractor.
		return

	# Come up with a destination directory for this file.
	dest_subdir = scan_dir_path[subdir_trim_index:]
	while dest_subdir[:len(os.sep)] == os.sep:
		dest_subdir = dest_subdir[len(os.sep):]
	dest_file_path = os.path.join(dest_subdir, scan_file_name + ':')
	dest_dir = os.path.join(next_dir_number_path, dest_file_path)
	dest_dir_0 = os.path.join(os.path.dirname(next_dir_number_path), '0', dest_file_path)

	# Run through file extractors until one succeeds.
	for extractor in file_extractors:
		# Run the extractor.
		try:
			extractor_result = extractor.extract(file_path, file_data, dest_dir, dest_dir_0)
		except extractors.MultifileStaleException:
			# This file has gone missing between the multi-file lock being
			# requested and successfully acquired. Stop extraction immediately.
			break
		except Exception as e:
			if util.raise_enospc and getattr(e, 'errno', None) == errno.ENOSPC:
				# Abort on no space if requested.
				print('{0} => aborting extraction due to disk space\n'.format(file_path[path_trim_index:]), end='')
				raise

			# Log an error.
			util.log_traceback('extracting', file_path)
			continue
		finally:
			if extractor.multifile_locked:
				extractor.multifile_locked = False
				extractor.multifile_lock.release()

		# Check if the extractor produced any results.
		if extractor_result:
			# Handle the line break ourselves, since Python prints the main
			# body and line break separately, causing issues when multiple
			# threads/processes are printing simultaneously.
			print('{0} => {1}{2}\n'.format(file_path[path_trim_index:], extractor.__class__.__name__, (extractor_result == True) and ' (skipped)' or ''), end='')
			break

	# Remove destination directories if they were created but are empty.
	for to_remove in (dest_dir, dest_dir_0):
		util.rmdirs(to_remove)

def extract_process(queue, abort_flag, multifile_lock, dir_number_path, next_dir_number_path, options):
	"""Main loop for the extraction multiprocessing pool."""

	# Set up extractors.
	image_extractor = extractors.ImageExtractor()
	if options['unpack-only']:
		file_extractors = []
	else:
		file_extractors = [
			extractors.DiscardExtractor(),
		]
	file_extractors += [
		extractors.ISOExtractor(),
		extractors.VMExtractor(),
		extractors.PEExtractor(),
		extractors.ASTExtractor(),
		extractors.FATExtractor(),
		extractors.MBRSafeExtractor(),
		extractors.TarExtractor(),
		extractors.ArchiveExtractor(),
		extractors.CPUZExtractor(),
		extractors.HexExtractor(),
		image_extractor,
		extractors.ApricotExtractor(),
		extractors.IntelNewExtractor(),
	]
	if not options['unpack-only']:
		file_extractors += [
			extractors.DellExtractor(),
		]
	file_extractors += [
		extractors.IntelExtractor(),
		extractors.OCFExtractor(),
		extractors.OMFExtractor(),
		extractors.TrimondExtractor(),
		extractors.InterleaveExtractor(),
	]
	if not options['unpack-only']:
		file_extractors += [
			extractors.BIOSExtractor(),
			extractors.UEFIExtractor(),
		]
	file_extractors += [
		extractors.MBRUnsafeExtractor(),
	]

	# Disable debug mode and add a reference to some common objects on all extractors.
	dummy_func = lambda self, *args: None
	for extractor in file_extractors:
		extractor.multifile_lock = multifile_lock
		extractor.image_extractor = image_extractor
		if not options['debug']:
			extractor.debug = False
			extractor.debug_print = dummy_func

	# Raise exceptions on no space if requested.
	util.raise_enospc = options['enospc']

	# Cache trim index values for determining a file's relative paths.
	dir_number_path = dir_number_path.rstrip(os.sep)
	subdir_trim_index = len(dir_number_path)
	path_trim_index = len(os.path.dirname(dir_number_path)) + len(os.sep)

	# Receive work from the queue.
	while True:
		item = queue.get()
		if item == None: # special item to stop the loop
			break
		elif abort_flag.value:
			continue
		try:
			extract_file(file_extractors, subdir_trim_index, path_trim_index, next_dir_number_path, *item)
		except Exception as e:
			if util.raise_enospc and getattr(e, 'errno', None) == errno.ENOSPC:
				# Abort all threads if ENOSPC was raised.
				abort_flag.value = 1
				continue
			raise

async def extract(dir_path, _, options):
	"""Main function for extraction."""

	# Check if the structure is correct.
	if not os.path.exists(os.path.join(dir_path, '1')):
		print('Incorrect directory structure. All data to unpack should be located inside', file=sys.stderr)
		print('a directory named 1 in turn located inside the given directory.', file=sys.stderr)
		return 2

	# Check if bios_extract is there.
	if not os.path.exists(os.path.abspath(os.path.join('bios_extract', 'bios_extract'))):
		print('bios_extract binary not found, did you compile it?', file=sys.stderr)
		return 3

	# Open devnull file for shell command output.
	devnull = open(os.devnull, 'wb')

	# Recurse through directory numbers.
	dir_number = 1
	while True:
		dir_number_path = os.path.join(dir_path, str(dir_number))
		next_dir_number_path = os.path.join(dir_path, str(dir_number + 1))

		# Fix permissions on extracted archives.
		print('Fixing up directory {0}:'.format(dir_number), end=' ', flush=True)
		try:
			print('chown', end=' ', flush=True)
			subprocess.run(['chown', '-hR', '--reference=' + dir_path, '--', dir_number_path], stdout=devnull, stderr=subprocess.STDOUT)
			print('chmod', end=' ', flush=True)
			subprocess.run(['chmod', '-R', 'u+rwx', '--', dir_number_path], stdout=devnull, stderr=subprocess.STDOUT) # execute for listing directories
		except:
			pass
		print()

		# Start multiprocessing pool.
		print('Starting extraction on directory {0}'.format(dir_number), end='', flush=True)
		queue_size = options['threads'] + len(options['remote_servers'])
		queue = multiprocessing.Queue(maxsize=queue_size * 8)
		abort_flag = multiprocessing.Value('B', 0)
		initargs = (queue, abort_flag, multiprocessing.Lock(), dir_number_path, next_dir_number_path, options)
		mp_pool = multiprocessing.Pool(options['threads'], initializer=extract_process, initargs=initargs)
		print(flush=True)

		# Create next directory.
		if not os.path.isdir(next_dir_number_path):
			os.makedirs(next_dir_number_path)

		# Scan directory structure.
		found_any_files = False
		for scan_dir_path, scan_dir_names, scan_file_names in os.walk(dir_number_path):
			for scan_file_name in scan_file_names:
				found_any_files = True
				queue.put((scan_dir_path, scan_file_name))
				if abort_flag.value: # stop feeding queue if a thread abort was requested
					break

		# Stop if no files are left.
		if not found_any_files:
			# Remove this directory and the directory if they're empty.
			try:
				os.rmdir(dir_number_path)
				dir_number -= 1
			except:
				pass
			try:
				os.rmdir(next_dir_number_path)
			except:
				pass
			break

		# Increase number.
		dir_number += 1

		# Stop multiprocessing pool and wait for its workers to finish.
		for _ in range(queue_size):
			queue.put(None)
		mp_pool.close()
		mp_pool.join()

		# Abort extraction if a thread abort was requested.
		if abort_flag.value:
			return 1

	# Create 0 directory if it doesn't exist.
	print('Merging directories:', end=' ')
	merge_dest_path = os.path.join(dir_path, '0')
	if not os.path.isdir(merge_dest_path):
		os.makedirs(merge_dest_path)

	# Merge all directories into the 0 directory.
	for merge_dir_name in range(1, dir_number + 1):
		merge_dir_path = os.path.join(dir_path, str(merge_dir_name))
		if not os.path.isdir(merge_dir_path):
			continue
		print(merge_dir_name, end=' ')

		subprocess.run(['cp', '-rlaT', merge_dir_path, merge_dest_path], stdout=devnull, stderr=subprocess.STDOUT)
		subprocess.Popen(['rm', '-rf', merge_dir_path], stdout=devnull, stderr=subprocess.STDOUT)

	# Clean up.
	devnull.close()
	print()
	return 0


# Analysis module.

amipci_pattern = re.compile('''amipci_([0-9A-F]{4})_([0-9A-F]{4})\\.rom$''')

def analyze_files(formatter, scan_base, file_analyzers, scan_dir_path, scan_file_names):
	"""Process the given files for analysis."""

	# Set up caches.
	files_flags = {}
	files_data = {}
	combined_oroms = []
	header_data = None

	# In combined mode (enabled by InterleaveExtractor and BIOSExtractor), we
	# handle all files in the directory as a single large blob, to avoid any doubts.
	combined = ':combined:' in scan_file_names
	if combined:
		files_data[''] = b''

	# Sort file names for better predictability. The key= function forces
	# "original.tm1" to be combined after "original.tmp" for if the Award
	# identification data spans across both files (AOpen AX6B(+) R2.00)
	if len(scan_file_names) > 1:
		scan_file_names.sort(key=lambda fn: (fn == 'original.tm1') and 'original.tmq' or fn)

	# Read files into the cache.
	cache_quota = ANALYZER_MAX_CACHE_MB * 1073741824
	for scan_file_name in scan_file_names:
		# Read up to 16 MB as a safety net.
		file_data = util.read_complement(os.path.join(scan_dir_path, scan_file_name))

		# Write data to cache.
		if scan_file_name == ':header:':
			header_data = file_data
		elif combined and scan_file_name != ':combined:':
			files_data[''] += file_data

			# Add PCI option ROM IDs extracted from AMI BIOSes by bios_extract, since the ROM might not
			# contain a valid PCI header to begin with. (Apple PC Card with OPTi Viper and AMIBIOS 6)
			match = amipci_pattern.match(scan_file_name)
			if match:
				combined_oroms.append((int(match.group(1), 16), int(match.group(2), 16)))
		else:
			files_data[scan_file_name] = file_data

		# Stop reading if the cache has gotten too big.
		cache_quota -= len(file_data)
		if cache_quota <= 0:
			break

	# Prepare combined-mode analysis.
	if combined:
		# Set interleaved flag on de-interleaved blobs.
		try:
			flag_size = os.path.getsize(os.path.join(scan_dir_path, ':combined:'))
			if flag_size >= 2:
				combined = 'Interleaved'
				if flag_size > 2:
					combined += str(flag_size)
		except:
			pass

		# Commit to only analyzing the large blob.
		scan_file_names = ['']
	elif header_data:
		# Remove header flag file from list.
		scan_file_names.remove(':header:')

	# Analyze each file.
	for scan_file_name in scan_file_names:
		# Read file from cache if possible.
		scan_file_path = os.path.join(scan_dir_path, scan_file_name)
		file_data = files_data.get(scan_file_name, None)
		if file_data == None:
			# Read up to 16 MB as a safety net.
			file_data = util.read_complement(scan_file_path)

		# Check for an analyzer which can handle this file.
		analyzer_file_path = combined and scan_dir_path or scan_file_path
		bonus_analyzer_metadata = bonus_analyzer_oroms = None
		file_analyzer = None
		strings = None
		for analyzer in file_analyzers:
			# Reset this analyzer.
			analyzer.reset()
			analyzer._file_path = scan_file_path

			# Check if the analyzer can handle this file.
			try:
				analyzer_result = analyzer.can_handle(analyzer_file_path, file_data, header_data)
			except:
				# Log an error.
				util.log_traceback('searching for analyzers for', os.path.join(scan_dir_path, scan_file_name))
				continue

			# Move on if the analyzer responded negatively.
			if not analyzer_result:
				# Extract metadata and option ROMs from the bonus analyzer.
				if bonus_analyzer_metadata == None:
					bonus_analyzer_metadata = analyzer.metadata
					bonus_analyzer_oroms = analyzer.oroms
				continue

			# Run strings on the file data if required (only once if requested by analyzer).
			if analyzer.can_analyze():
				if not strings:
					try:
						strings = subprocess.run(['strings', '-n8'], input=file_data, stdout=subprocess.PIPE).stdout.decode('ascii', 'ignore').split('\n')
					except:
						util.log_traceback('running strings on', os.path.join(scan_dir_path, scan_file_name))
						continue

				# Analyze each string.
				try:
					for string in strings:
						analyzer.analyze_line(string)
				except analyzers.AbortAnalysisError:
					# Analysis aborted.
					pass
				except:
					# Log an error.
					util.log_traceback('analyzing', os.path.join(scan_dir_path, scan_file_name))
					continue

			# Take this analyzer if it produced a version.
			if analyzer.version:
				# Clean up version field if an unknown version was returned.
				if analyzer.version == '?':
					analyzer.version = ''

				# Stop looking for analyzers.
				file_analyzer = analyzer
				break

		# Did any analyzer successfully handle this file?
		if not file_analyzer:
			# Treat this as a standalone PCI option ROM file if BonusAnalyzer found any.
			if bonus_analyzer_oroms:
				bonus_analyzer_metadata = []
				file_analyzer = file_analyzers[0]
			else:
				# Move on to the next file if nothing else.
				continue

		# Add interleaved flag to metadata.
		if type(combined) == str:
			bonus_analyzer_metadata.append(('ROM', combined))

		# Clean up the file path.
		scan_file_path_full = os.path.join(scan_dir_path, scan_file_name)

		# Remove combined directories from the path.
		found_flag_file = True
		while found_flag_file:
			# Find archive indicator.
			archive_index = scan_file_path_full.rfind(':' + os.sep)
			if archive_index == -1:
				break

			# Check if a combined or header flag file exists.
			found_flag_file = False
			for flag_file in (':combined:', ':header:'):
				if os.path.exists(os.path.join(scan_file_path_full[:archive_index] + ':', flag_file)):
					# Trim the directory off.
					scan_file_path_full = scan_file_path_full[:archive_index]
					found_flag_file = True
					break

		scan_file_path = scan_file_path_full[len(scan_base) + len(os.sep):]

		# Remove root extraction directory.
		slash_index = scan_file_path.find(os.sep)
		if slash_index == 1 and scan_file_path[0] == '0':
			scan_file_path = scan_file_path[2:]

		# De-duplicate and sort metadata and option ROMs.
		metadata = list(set('[{0}] {1}'.format(key, value.replace('\n', '\n' + (' ' * (len(key) + 3)))).strip() for key, value in (analyzer.metadata + bonus_analyzer_metadata)))
		metadata.sort()
		oroms = list(set(combined_oroms + analyzer.oroms + bonus_analyzer_oroms))
		oroms.sort()

		# Add names to option ROMs.
		previous_vendor = previous_device = None
		for x in range(len(oroms)):
			if oroms[x][0] == -1 and type(oroms[x][1]) == str: # generic ROM
				# Format string.
				oroms[x] = '[{1}] {2}'.format(*oroms[x]).replace('\n', '\n' + (' ' * (len(oroms[x][1]) + 3)))
			elif len(oroms[x]) == 2: # PCI ROM
				# Get vendor and device IDs and names.
				vendor_id, device_id = oroms[x]
				vendor, device = util.get_pci_id(vendor_id, device_id)

				# Skip valid vendor IDs associated to a bogus device ID.
				if device == '[Unknown]' and device_id == 0x0000:
					oroms[x] = None
					continue

				# Clean up IDs.
				vendor = util.clean_vendor(vendor).strip()
				device = util.clean_device(device, vendor).strip()

				# De-duplicate vendor names.
				if vendor == previous_vendor and vendor != '[Unknown]':
					if device == previous_device:
						previous_device, device = device, ''
						previous_vendor, vendor = vendor, '\u2196' # up-left arrow
					else:
						previous_device = device
						previous_vendor, vendor = vendor, ' ' * len(vendor)
				else:
					previous_device = device
					previous_vendor = vendor

				# Format string.
				oroms[x] = '[{0:04x}:{1:04x}] {2} {3}'.format(vendor_id, device_id, vendor, device)
			else: # PnP ROM
				# Get PnP ID, vendor name and device name.
				device_id, vendor, device = oroms[x]

				# Extract ASCII letters from the PnP ID.
				pnp_id = ''.join(chr(0x40 + (letter & 0x1f)) for letter in (device_id >> 26, device_id >> 21, device_id >> 16))

				# Add the numeric part of the PnP ID.
				pnp_id += format(device_id & 0xffff, '04x').upper()

				# Clean up vendor and device names.
				vendor_device = ((vendor or '') + '\n' + (device or '')).replace('\r', '')
				vendor_device = '\n'.join(x.strip() for x in vendor_device.split('\n') if x.strip())

				# Format string.
				oroms[x] = '[{0}] {1}'.format(pnp_id, vendor_device.replace('\n', '\n' + (' ' * (len(pnp_id) + 3))))

		# Remove bogus option ROM device ID entries.
		while None in oroms:
			oroms.remove(None)

		# Add file name in single-file analysis.
		if not scan_dir_path and not scan_file_path:
			scan_file_path = os.path.basename(scan_base)

		# Collect the analyzer's results.
		fields = [((type(field) == str) and field.replace('\t', ' ').strip() or field) for field in [
			scan_file_path,
			file_analyzer.vendor,
			file_analyzer.version,
			formatter.split_if_required('\n', file_analyzer.string),
			formatter.split_if_required('\n', file_analyzer.signon),
			formatter.join_if_required('\n', metadata),
			formatter.join_if_required('\n', oroms),
		]]
		return fields
def analyze_process(queue, formatter, scan_base, options):
	"""Main loop for the analysis multiprocessing pool."""

	# Set up analyzers.
	file_analyzers = [
		analyzers.BonusAnalyzer(), # must be the first one
		analyzers.AwardPowerAnalyzer(), # must run before AwardAnalyzer
		analyzers.ToshibaAnalyzer(), # must run before AwardAnalyzer
		analyzers.AwardAnalyzer(), # must run before PhoenixAnalyzer
		analyzers.QuadtelAnalyzer(), # must run before PhoenixAnalyzer
		analyzers.PhoenixAnalyzer(), # must run before AMIDellAnalyzer and AMIIntelAnalyzer
		analyzers.AMIUEFIAnalyzer(), # must run before AMIAnalyzer
		analyzers.AMIAnalyzer(), # must run before AMIIntelAnalyzer
		analyzers.AMIIntelAnalyzer(),
		analyzers.MRAnalyzer(),
		# less common BIOSes with no dependencies on the common part begin here #
		analyzers.AcerAnalyzer(),
		analyzers.AcerMultitechAnalyzer(),
		analyzers.AmproAnalyzer(),
		analyzers.AmstradAnalyzer(),
		analyzers.CDIAnalyzer(),
		analyzers.CentralPointAnalyzer(),
		analyzers.ChipsAnalyzer(),
		analyzers.CommodoreAnalyzer(),
		analyzers.CompaqAnalyzer(),
		analyzers.CopamAnalyzer(),
		analyzers.CorebootAnalyzer(),
		analyzers.DTKGoldStarAnalyzer(),
		analyzers.GeneralSoftwareAnalyzer(),
		analyzers.IBMSurePathAnalyzer(),
		analyzers.IBMAnalyzer(),
		analyzers.ICLAnalyzer(),
		analyzers.InsydeAnalyzer(),
		analyzers.IntelUEFIAnalyzer(),
		analyzers.JukoAnalyzer(),
		analyzers.MRAnalyzer(),
		analyzers.MylexAnalyzer(),
		analyzers.OlivettiAnalyzer(),
		analyzers.PhilipsAnalyzer(),
		analyzers.PromagAnalyzer(),
		analyzers.SchneiderAnalyzer(),
		analyzers.SystemSoftAnalyzer(),
		analyzers.TandonAnalyzer(),
		analyzers.TinyBIOSAnalyzer(),
		analyzers.WhizproAnalyzer(),
		analyzers.ZenithAnalyzer(),
	]

	logger = logging.getLogger('app')
	# Disable debug mode on all analyzers.
	if not options['debug']:
		dummy_func = lambda self, *args: None
		for analyzer in file_analyzers:
			analyzer.debug_print = dummy_func
			analyzer.debug = False
	# Receive work from the queue.
	while True:
		item = queue.get()
		if item == None:
			break
		mything = analyze_files(formatter, scan_base, file_analyzers, *item)
		if mything is not None:
			logger.info(f'{mything}')


async def analyze(dir_path, formatter_args, options):
	"""Main function for analysis."""
	# Initialize output formatter.
	output_formats = {
		'csv': (formatters.XSVFormatter, ','),
		'scsv': (formatters.XSVFormatter, ';'),
		'json': formatters.JSONObjectFormatter,
		'jsontable': formatters.JSONTableFormatter,
	}
	formatter = output_formats.get(options['format'], None)
	if not formatter:
		raise Exception('unknown output format ' + options['format'])
	if type(formatter) == tuple:
		formatter = formatter[0](*formatter[1:], sys.stdout, options, formatter_args)
	else:
		formatter = formatter(sys.stdout, options, formatter_args)
	formatter.begin()
	# Remove any trailing slash from the root path, as the output path cleanup
	# functions rely on it not being present.
	if dir_path[-len(os.sep):] == os.sep:
		dir_path = dir_path[:-len(os.sep)]
	elif dir_path[-1:] == '/':
		dir_path = dir_path[:-1]
	# logging stuff
	logger = logging.getLogger('app')
	logger.setLevel(logging.DEBUG)
	log_capture_string = io.StringIO()
	fh = logging.FileHandler('strings.log')
	log_formatter = logging.Formatter('%(message)s')
	fh.setFormatter(log_formatter)
	logger.addHandler(fh)
	# Start multiprocessing pool.
	queue = multiprocessing.Queue(maxsize=options['threads'] * 8)
	mp_pool = multiprocessing.Pool(options['threads'], initializer=analyze_process, initargs=(queue, formatter, dir_path, options))

	if os.path.isdir(dir_path):
		# Scan directory structure.
		for scan_dir_path, scan_dir_names, scan_file_names in os.walk(dir_path):
			if ':combined:' in scan_file_names or ':header:' in scan_file_names: # combined mode: process entire directory at once
				queue.put((scan_dir_path, scan_file_names))
			else: # regular mode: process individual files
				for scan_file_name in scan_file_names:
					queue.put((scan_dir_path, [scan_file_name]))
	else:
		# Scan single file.
		queue.put(('', [dir_path]))

	# Stop multiprocessing pool and wait for its workers to finish.
	for _ in range(options['threads']):
		queue.put(None)
	mp_pool.close()
	mp_pool.join()
	print(log_capture_string.getvalue())
	global lines
	with open('strings.log') as file2:
		for line in file2:
			lines.append(literal_eval(line))
	if os.path.exists('strings.log'):
		os.remove('strings.log')
	return 0

# ----  BOT BEGINS HERE -------
def input_path(messageid):
    return str("roms/" + str(messageid) + "/1")

def output_path(messageid):
    return str("roms/" + str(messageid) + "/0")

def message_path(messageid):
    return str("roms/" + str(messageid) + "/")

# prepares the folders for bios-tools
async def folderMaintenance(messageid):
	global status
	status = 0
	# creates /1 if it doesn't exist already
	if not os.path.exists(input_path(messageid)):
		os.makedirs(input_path(messageid))
	status += 1

# cleans up after bios-tools
async def folderCleanup(messageid):
	global status
	if status == 2:
		if os.path.exists(message_path(messageid)):
			shutil.rmtree(message_path(messageid))

# spreadsheet handler
def csv_process(ctx: commands.Context, lines, csv):
	sheet = open(message_path(ctx.message.id) + "/output.csv", "w")
	for i in range (0, len(lines)):
		item = ""
		for j in range (0, len(lines[i])):
			content = str(lines[i][j])
			item += "\"" + content + "\""
			if j < 6:
				item += ","
			else:
				item += "\n"
		sheet.write(item)
	sheet.close()
	if csv is False:
		newname = message_path(ctx.message.id) + "/TRW_" + ctx.message.author.name + "_" + datetime.now().strftime("%Y%m%d_%H%M%S") + ".csv"
		copy(message_path(ctx.message.id) + "/output.csv", newname)
		return newname
	newname = message_path(ctx.message.id) + "/TRW_" + ctx.message.author.name + "_" + datetime.now().strftime("%Y%m%d_%H%M%S") + ".xlsx"
	df = pd.read_csv(message_path(ctx.message.id) + '/output.csv')
	writer = pd.ExcelWriter(newname)
	df.to_excel(writer, index = None, header=True, sheet_name='BIOS_analysis')
	# Auto-adjust columns' width
	for column in df:
		column_width = max(df[column].astype(str).map(len).max(), len(column))
		col_idx = df.columns.get_loc(column)
		writer.sheets['BIOS_analysis'].set_column(col_idx, col_idx, column_width + 3)
	writer.save()
	wb= openpyxl.load_workbook(newname)
	ws = wb.active
	ws.auto_filter.ref = ws.dimensions
	wb.save(newname)
	return newname

# text output handler for a single file
def format_response(lines):
	string = "```\n"
	for i in range (0, len(lines[0])):
		header = str(lines[0][i])
		content = str(lines[1][i])
		#very specific empty strings, used just for visual formatting
		if i == 0:
			header += ":     "
		if i == 1 or i == 3:
			header += ":   "
		if i == 2 or i == 4:
			header += ":  "
		if i == 5:
			header += ": "
			content = content.replace("\n", "\n          ")
		if i == 6:
			header += ": "
			content = content.replace("\n", "\n      ")
		string += header + content + "\n"
	string += "```"
	return string

# text output handler for multiple files
def format_multiresponse(lines):
	string = "```\nTRW BIOS analyze (!bns):\n__________________________________________________________\n"
	for i in range (1, len(lines)):
		for j in range (0, len(lines[0])):
			header = "[" + str(i) + "] " + str(lines[0][j])
			content = str(lines[i][j])
			#very specific empty strings, used just for visual formatting
			if j == 0:
				header += ":    "
				string += header + content + "\n"
			if j == 1:
				header = "[" + str(i) + "] Type :   "
				content = str(lines[i][j]) + " " + str(lines[i][j+1])
				string += header + content + "\n"
			if j == 3:
				header += ":  "
				string += header + content + "\n"
			if j == 4:
				header += ": "
				string += header + content + "\n"
				if i < len(lines)-1:
					string += "__________________________________________________________\n"
	string += "```"
	return string

# fetches one or more files from a URL list
async def getURL(ctx: commands.Context, urls):
	file_count = 0
	# prevent duplicate downloads with a unique url list
	for url in list(set(urls)):
		try:
			with requests.get(url, stream=True) as r:
				filename = ""
				#print(r.headers)
				if "Content-Length" in r.headers.keys():
					if int(r.headers["Content-Length"]) > 104857600:
						break
				if "Content-Disposition" in r.headers.keys():
					fname = re.findall("filename=*(.+)", r.headers["Content-Disposition"])
					if fname != []:
						if "*=UTF-8" in fname[0]:
							filename = fname[0][9:]
						else:
							filename = fname[0]
					else:
						filename = url.split("/")[-1]
				else:
					filename = url.split("/")[-1]
			#print(url)
			if filename == "":
				filename = "unknown"
			flen = filename.split(".")
			# try to download files
			try:
				sema = asyncio.BoundedSemaphore(5)
				async with sema, aiohttp.ClientSession() as session:
					async with session.get(url) as resp:
						assert resp.status == 200
						data = await resp.read()
				async with async_open(os.path.join(input_path(ctx.message.id), filename), "wb") as outfile:
					await outfile.write(data)
				file_count += 1
			except Exception as e:
				print(e)
		except Exception as ex:
			print(ex)
	return file_count

# message attachment/link handler
async def getAttachments(ctx: commands.Context):
	global status
	file_count = 0
	# saves all the attachments
	if ctx.message.attachments:
		for attach in ctx.message.attachments:
			file_count += 1
			await attach.save(input_path(ctx.message.id) + "/" + attach.filename)
	# try to download all the files if linked
	if 'https://' in ctx.message.content or 'http://' in ctx.message.content:
		if 'discord.com/channels' in ctx.message.content:
			await ctx.reply("You can only use direct links or attachments", mention_author=False)
		else:
			urls = re.findall('(?P<url>https?://[^\s]+)', ctx.message.content)
			file_count = await getURL(ctx, urls)
	if file_count > 0:
		status += 1

# extract and analyze BIOSes
async def extract_analyze(ctx: commands.Context, botMessage):
	global lines
	lines = [["File","Vendor","Version","String","Sign-on","Metadata","ROMs"]]
	inPath = message_path(ctx.message.id)
	#print(inPath)
	global status
	if status == 2:
		options = {
			'array': False,
			'debug': False,
			'enospc': False,
			'format': 'csv',
			'headers': True,
			'hyperlink': False,
			'threads': os.cpu_count() or 4,
			'unpack-only': False,
			'docker-usage': False,
			'remote_servers': [],
			'remote_port': DEFAULT_REMOTE_PORT,
		}
		# message update for each stage of the process
		await botMessage.edit(content="Extracting BIOS modules")
		await extract(inPath, 'csv', options)
		await botMessage.edit(content="Analyzing BIOS modules")
		await analyze(inPath, 'csv', options)
		lines[1:] = sorted(lines[1:], key = lambda x: x[0])
	else:
		botMessage = await ctx.reply("Error getting files", mention_author=False)
# extract BIOSes
async def extract_only(ctx: commands.Context, botMessage):
	inPath = message_path(ctx.message.id)
	global status
	if status == 2:
		options = {
			'array': False,
			'debug': False,
			'enospc': False,
			'format': 'csv',
			'headers': True,
			'hyperlink': False,
			'threads': os.cpu_count() or 4,
			'unpack-only': False,
			'docker-usage': False,
			'remote_servers': [],
			'remote_port': DEFAULT_REMOTE_PORT,
		}
		# message update for each stage of the process
		await botMessage.edit(content="Extracting BIOS modules")
		await extract(inPath, 'csv', options)
	else:
		botMessage = await ctx.reply("Error getting files", mention_author=False)

# sends oputput message to Discord
async def sendMsgGuild(ctx: commands.Context, botMessage, msg):
	if len(msg) > 2000:
		messageFile = open(message_path(ctx.message.id) + "/output.txt", "w")
		messageFile.write(msg[4:len(msg)-3])
		messageFile.close()
		discordFile = discord.File(message_path(ctx.message.id) + '/output.txt')
		await botMessage.edit(content="Output is too big, attached as text file.")
		await botMessage.add_files(discordFile)
		os.remove(message_path(ctx.message.id) + '/output.txt')
	else:
		await botMessage.edit(content=msg)

# processes BIOSes for output to Discord
async def processFiles(ctx: commands.Context, sheet, botMessage, csv):
	await extract_analyze(ctx,botMessage)
	global lines
	# checking if the analyze step was succesful
	if len(lines) > 1:
		# if a single file was processed, just send a discord text reply
		if len(lines) == 2:
			await sendMsgGuild(ctx, botMessage, format_response(lines))
		# if there are more files, make a decision
		else:
			if sheet is True:
				# send a spreadsheet with the results
				newname = csv_process(ctx, lines, csv)
				output_sheet = discord.File(newname)
				await botMessage.edit(content="Multiple files processed, here are the results:")
				await botMessage.add_files(output_sheet)
				# removes previous sheets, if they exists
				if os.path.exists(message_path(ctx.message.id) + '/output.csv'):
					os.remove(message_path(ctx.message.id) + '/output.csv')
				if os.path.exists(newname):
					os.remove(newname)
			else:
				# the bot will reply with a simplified multi row message with the essentials
				await sendMsgGuild(ctx, botMessage, format_multiresponse(lines))

	# for some reason the analyze function did not return info, cancels any further actions of the bot
	else:
		await botMessage.edit(content="Oops, the files were not recognized")
def findImage(path):
	result = []
	for root, dirs, files in os.walk(path):
		if "image.jpg" in files:
			result.append(os.path.join(root, "image.jpg"))
		if "image.png" in files:
			result.append(os.path.join(root, "image.png"))
	return result
# processes BIOS EPA for output to Discord
async def processEpa(ctx: commands.Context, sheet, botMessage):
	await extract_only(ctx,botMessage)
	imagepath = []
	imagepath = findImage(message_path(ctx.message.id) + "0")
	if imagepath != []:
		await botMessage.edit(content="Here are the images:")
		for path in imagepath:
			output_file = discord.File(path)
			await ctx.send(file=output_file)
			print(path)
	else:
		await botMessage.edit(content="Oops, the files were not recognized")
def format_out(lines, i):
	a_string = ""
	for j in range (0, len(lines[0])):
		a_header = "[" + str(i) + "] " + str(lines[0][j])
		a_content = str(lines[i][j])
		#very specific empty strings, used just for visual formatting
		if j == 0:
			a_header = "[" + str(i) + "] File path: "
			a_string += a_header + a_content + "\n"
		if j == 1:
			a_header = "[" + str(i) + "] Type :     "
			a_content = str(lines[i][j]) + " " + str(lines[i][j+1])
			a_string += a_header + a_content + "\n"
		if j == 3:
			a_header += ":    "
			a_string += a_header + a_content + "\n"
		if j == 4:
			a_header += ":   "
			a_string += a_header + a_content + "\n"
	return a_string
# checks the presence of BIOSes on TRW
async def checkTRW(ctx: commands.Context, botMessage):
	await extract_analyze(ctx,botMessage)
	global lines
	if len(lines) > 1:
		await botMessage.edit(content="Checking if they exist on TRW")
		string = "```\nTRW BIOS upload check (!exist):\n__________________________________________________________\n"
		ids = ""
		for i in range (1, len(lines)):
			# get file name
			if len(str(lines[i][0]).split("/")) > 1:
				fname1 = str(lines[i][0]).split("/")[0][1:-1]
				fname2 = str(lines[i][0]).split("/")[-1]
			else:
				fname1 = str(lines[i][0])
				fname2 = str(lines[i][0])
			# send request with post string
			header = "[" + str(i) + "] " + fname2
			content = ""
			r = requests.post("https://dev.theretroweb.com/bios/bot/string", data={'string': str(lines[i][3]), 'filename1': fname1, 'filename2': fname2})
			if r.status_code == 200:
				results = json.loads(r.text)
				if len(results) < 1:
					#not uploaded
					header += ": NOT uploaded on TRW\n----------------------------------------------------------\n"
				else:
					#we have results, process them
					trw_info = "[" + str(i) + "] TRW info:  "
					if len(results) == 1:
						# we have one result
						ids += str(results[0]['id']) + ", "
						if results[0]['file'] is None:
							header += ": recorded on TRW, NOT backed up\n----------------------------------------------------------\n"
						else:
							header += ": uploaded on TRW\n----------------------------------------------------------\n"
						content = trw_info + "[board ID: " + ids[:-2] + "] " + str(results[0]['manufacturer']) + " " + str(results[0]['name']) + " " + str(results[0]['version']) + "\n"
					if len(results) > 1:
						# we have many results, double check with BIOS versions
						content = trw_info
						flag = 0
						newresults = []
						for item in results:
							ids += str(item['id']) + ", "
							if item['file'] is None:
								flag += 1
							if item['version'] in lines[i][4]:
								newresults.append(item)
						if flag == 0:
							header += ": uploaded on TRW (multiple matches)\n----------------------------------------------------------\n"
						if flag < len(results):
							header += ": some are uploaded on TRW (multiple matches)\n----------------------------------------------------------\n"
						if flag == len(results):
							header += ": recorded on TRW, NOT backed up (multiple matches)\n----------------------------------------------------------\n"
						if len(newresults) == 0:
							# couldn't determine based on version, throw them all in
							content += "[board IDs: " + ids[:-2] + "] versions do not match bot analysis, check each board manually\n"
						if len(newresults) == 1:
							# narrowed down to one entry
							ids += str(newresults[0]['id']) + ", "
							content += "[board ID: " + str(newresults[0]['id']) + " " + str(newresults[0]['manufacturer']) + " check each board manually\n"
						if len(newresults) > 1:
							# multiple files with the same version
							content += "[board IDs: " + ids[:-2] + "] multiple similar versions, check each board manually\n"
				content += format_out(lines, i)
			else:
				content += ": something went wrong on the server :("
				print(r.text)
				print(lines[i][3], fname1, fname2)
			string += header + content
			if i < len(lines) - 1:
				string += "__________________________________________________________\n"
		string += "```"
		if ids != "":
			string += "TRW Links: "
			for bid in ids.split(", ")[:-1]:
				string += "[" + bid + "](<https://theretroweb.com/motherboards/" + bid + "#downloads>), "
			string = string[:-2] + "."
		await sendMsgGuild(ctx, botMessage, string)
	# for some reason the analyze function did not return info, cancels any further actions of the bot
	else:
		await botMessage.edit(content="Oops, the files were not recognized")

async def findManuf(manuf):
	r = requests.get("https://theretroweb.com/bios/infoadv")
	text = r.text
	idx = text.find(manuf + ":") + 5
	if len(manuf) == 2:
		while ord(text[idx-6]) in range(48,57):
			idx = text[idx:].find(manuf + ":") + idx + 5
	if len(manuf) > 4:
		idx += 1
	idx2 = text[idx:].find("</li>")
	if idx != 4 and idx != 5:
		out = text[idx:(idx + idx2)].replace("\n","").strip()
		if(manuf == "00" or manuf == "9999"):
			out = out.replace("&lt;","<").replace("&gt;",">")
	else:
		out = "unknown"
	if len(manuf) < 5:
		out += " (" + manuf + ")"
	return out
def lbasupport(input):
	try:
		year = int(input[4:6])
		month = int(input[0:2])
		day = int(input[2:4])
		cutoff = datetime(1994, 7, 25)
		if 59 <= year <= 99:
			year = 1900 + year
		else:
			year = 2000 + year
		if datetime(year, month, day) < cutoff:
			return 0
		else:
			return 1
	except:
		return 2
async def determineStringContents(lines, pos):
	#determining the BIOS vendor
	vendor = lines[1]
	string = lines[3]
	cpuList = {"0": "8086/8088", "1": "unknown", "2": "286", "3": "386", "4": "486", "5":"Pentium (P5 family)", "6":"Pentium II (P6 family)", "7": "unknown (Olympus boards)", "X":"386SX or 486"}
	ROMSizeList = {"0":"64KB", "1":"128KB, 1MB or 2MB", "2":"256KB", "3":"512KB", "4":"1MB", "5":"2MB", "6":"4MB", "7":"8MB", "8":"16MB", "9":"32MB"}
	precolChipList = {
		"286":"generic 286",
		"307":"Chips & Technologies CS8236",
		"343":"VLSI VL82C286-SET TOPCAT",
		"386":"Texas Instruments TACT83000",
		"486":"Trigem 486",
		"AMI":"AMI 386",
		"AR2":"ALi M1207",
		"C&T":"Chips & Technologies 386",
		"EMI":"Elite e88C311/e88C312",
		"G22":"Headland GC101,GC102,GC103",
		"G23":"G2 386SX",
		"G2S":"GS62C201/202",
		"G2X":"Headland GCK133",
		"H12":"Headland HT12/+/A",
		"H1X":"Headland HT12/+/A",
		"INT":"discrete logic/Intel",
		"ISX":"ZyMOS POACH",
		"NET":"Chips & Technologies CS8221 NEAT",
		"NSX":"Chips & Technologies CS8281 NEATsx",
		"OC4":"OPTi 82C481/482",
		"OPB":"OPTi 82C381/382",
		"OPX":"OPTi 82C281",
		"OX3":"OPTi unknown",
		"PAQ":"COMPAQ 386/ZyMOS POACH",
		"PKD":"Chips & Technologies CS82310 (PEAK/DM 386 AT)",
		"S24":"Suntac ST62C241",
		"SC2":"Chips & Technologies 82C235 SCAT",
		"SIS":"SiS 85C310/320/330 Rabbit",
		"SUN":"Suntac ST62C00x",
		"VAX":"VIA Flex I/386SX",
		"VL2":"VLSI VL82CPCAT-16QC/-20QC",
		"VLX":" VLSI VL82CPCAT-QC or VL82CPCPM-16QC/20QC"
	}
	precolSetupList = {"D":"w/ Diagnostics","E":"Extended","S":"Normal"}
	awTypeList = {"1":"pre 4.5", "2":"4.5x Elite", "3":"PowerBIOS 5.0", "4":"Cardware PCMCIA", "5":"CAMPliant SCSI", "6":"6.0 Medallion"}
	awBusList = {"1":"ISA", "2":"MCA", "3":"EISA", "5":"EISA and ISA", "7":"unknown", "A":"PCI and ISA", "B":"PCI and EISA", "C":"ISA and PM", "D":"EISA and PM", "E":"PCI and PnP"}
	awCPUList = {"4":"486", "5":"Pentium (P5 family)", "6":"Pentium II (P6 family)", "9":"unknown", "U":"generic" }
	output = {}
	output["        File"] = lines[0]
	output["      String"] = string + "\n----------------------------------------------------------"
	try:
		if(vendor == "AMI"):
			if(len(string) > 20):
				if string[2] == '-' and string[6] == '-':
					print(string[0:6], string[6:])
					string = string[0:6] + '0' + string[6:]
					print(string[0:6], string[6:])
				if string[7] == '-' and string[13] == '-':
					string = string[0:8] + '0' + string[8:]
					print(string)
				output["        Type"] = "AMI color"
				output["  CPU family"] = cpuList[string[0]] if string[0] in cpuList else "unknown"
				output["    ROM size"] = ROMSizeList[string[1]] if string[1] in ROMSizeList else "unknown"
				output["    Revision"] = string[3:5] + "." + string[5:7]
				output["Manufacturer"] = await findManuf(string[10:14])
				output["Core version"] = string[24:30]
				output["  Identifier"] = string[31:]
				output[" LBA support"] = "Yes" if lbasupport(string[24:30]) else "No"
				output["       Flags"] = "--------------------------"
				output["            Halt on POST error"] = "Enabled" if int(string[15]) else "Disabled"
				output[" Initialize CMOS at every boot"] = "Enabled" if int(string[16]) else "Disabled"
				output["       Block pins 22,23 on KBC"] = "Enabled" if int(string[17]) else "Disabled"
				output["         Mouse support in BIOS"] = "Enabled" if int(string[18]) else "Disabled"
				output["Display keyboard error at POST"] = "Enabled" if int(string[19]) else "Disabled"
				output["   Display video error at POST"] = "Enabled" if int(string[20]) else "Disabled"
				output["  Display floppy error at POST"] = "Enabled" if int(string[21]) else "Disabled"
				output["     Wait for F1 error at POST"] = "Enabled" if int(string[22]) else "Disabled"
			elif(len(string) > 15):
				output["        Type"] = "AMI pre-color"
				if ("Ref. " in string):
					output["        Type"] = "AMI pre-color (Access Methods)"
				else:
					output["        Type"] = "AMI pre-color"
				output["       Setup"] = precolSetupList[string[0]] if string[0] in precolSetupList else "unknown"
				output["     Chipset"] = precolChipList[string[1:4]] if string[1:4] in precolChipList else "unknown"
				output["Manufacturer"] = await findManuf(string[5:9])
				output["Core version"] = string[10:16]
			else:
				output["        Type"] = "AMI unknown"
		elif(vendor == "Award"):
			awid_idx = string.rfind("-") + 1
			if len(string[awid_idx:]) < 3:
				awid_idx = string[:string.rfind("-")].rfind("-") + 1
			if awid_idx == 0:
				output["        Type"] = "Award unknown"
				return output
			output["        Type"] = "Award " + awTypeList[string[awid_idx]] if string[awid_idx] in awTypeList else "unknown"
			output["     Chipset"] = await findManuf(string[awid_idx + 2:awid_idx + 5].upper())
			output["  CPU family"] = awCPUList[string[awid_idx + 2]] if string[awid_idx + 2] in awCPUList else "unknown"
			output["    Bus type"] = awBusList[string[awid_idx + 1]] if string[awid_idx + 1] in awBusList else "unknown"
			output["Manufacturer"] = await findManuf(string[awid_idx + 5:awid_idx + 7].upper())
			output["Core version"] = lines[2]
			output["  Identifier"] = string[string.find("-") + 1:awid_idx - 1]
		elif(vendor == "Acer"):
			output["        Type"] = "Acer"
		else:
			output["        Type"] = "unknown"
	except Exception as Ex:
		output["Analysis error occurred"] = str(Ex)
	return output

# checks the string of BIOSes
async def checkString(ctx: commands.Context, botMessage):
	await extract_analyze(ctx,botMessage)
	global lines
	if len(lines) > 1:
		await botMessage.edit(content="Analysing POST strings")
		string = "```\nTRW BIOS string check - attachments (!string):\n__________________________________________________________\n"
		for i in range (1, len(lines)):
			output = await determineStringContents(lines[i],i)
			for key,value in output.items():
				string += "[" + str(i) + "] " + key + ": " + value + "\n"
			if i < len(lines)-1:
				string += "__________________________________________________________\n"
		string += "```"
		await sendMsgGuild(ctx, botMessage, string)
	else:
		await botMessage.edit(content="Oops, the files were not recognized")

# tries to find out the string vendor
async def guessString(ctx: commands.Context, botMessage):
	given_lines = []
	given_lines.append("N/A")
	strings = ctx.message.content.split()
	found = 0
	if(len(strings) < 2):
		await sendMsgGuild(ctx, botMessage, "```No text strings to check, skipping```")
		return
	for string in strings:
		#detect vendor
		st = string.strip()
		if("!string" in st or "http://" in st or "https://" in st):
			found += 1
			continue
		if(st[:10].find("/") != -1):
			given_lines.append("Award")
		elif(st[:5].find("-") != -1 or "Ref." in st):
			given_lines.append("AMI")
		elif("ACR" in st[:4]):
			given_lines.append("Acer")
		else:
			given_lines.append("unknown")
		given_lines.append("unknown (text only string)")
		given_lines.append(st)
		string = "```\nTRW BIOS string check - text input (!string):\n__________________________________________________________\n"
		output = await determineStringContents(given_lines,1)
		for key,value in output.items():
			string += key + ": " + value + "\n"
		string += "```"
		await sendMsgGuild(ctx, botMessage, string)
	if found == len(strings):
		await sendMsgGuild(ctx, botMessage, "```No text strings to check, skipping```")

# splits a BIOS file into ODD/EVEN
async def splitFiles(ctx: commands.Context):
	global status
	# saves all the attachments
	if ctx.message.attachments:
		if len(ctx.message.attachments) == 1:
			for attach in ctx.message.attachments:
				await attach.save(input_path(ctx.message.id) + "/" + attach.filename)
			botMessage = await ctx.reply("File split", mention_author=False)
			fn1 = input_path(ctx.message.id) + "/" + attach.filename[:attach.filename.rindex('.')] + "_LO.bin"
			fn2 = input_path(ctx.message.id) + "/" + attach.filename[:attach.filename.rindex('.')] + "_HI.bin"
			file0 = open(input_path(ctx.message.id) + "/" + attach.filename, "rb")
			file1 = open(fn1, "wb")
			file2 = open(fn2, "wb")
			byte = file0.read(1)
			file1.write(byte)
			byte = file0.read(1)
			file2.write(byte)
			while byte:
				byte = file0.read(1)
				file1.write(byte)
				byte = file0.read(1)
				file2.write(byte)
			file0.close()
			file1.close()
			file2.close()
			f1 = discord.File(fn1)
			f2 = discord.File(fn2)
			botMessage = await ctx.send("`EVEN (LOW)`")
			await botMessage.add_files(f1)
			botMessage2 = await ctx.send("`ODD (HIGH)`")
			await botMessage2.add_files(f2)
			status = 2
		else:
			await ctx.reply("Too many files attached", mention_author=False)
	else:
		botMessage = await ctx.reply("Error getting files", mention_author=False)

# joins interleaved BIOS files into one
async def joinFiles(ctx: commands.Context):
	global status
	preview = "Contents preview (first 512 characters):\n```"
	# saves all the attachments
	if ctx.message.attachments:
		if len(ctx.message.attachments) == 2:
			filename = []
			for attach in ctx.message.attachments:
				filename.append(input_path(ctx.message.id) + "/" + attach.filename)
				await attach.save(input_path(ctx.message.id) + "/" + attach.filename)
			botMessage = await ctx.reply("Files joined", mention_author=False)
			file0 = open(input_path(ctx.message.id) + "/OUT.bin", "wb")
			file1 = open(filename[1], "rb")
			file2 = open(filename[0], "rb")
			byte1 = file1.read(1)
			file0.write(byte1)
			byte2 = file2.read(1)
			file0.write(byte2)
			preview += byte1.decode("cp437")
			preview += byte2.decode("cp437")
			while byte1:
				byte1 = file1.read(1)
				file0.write(byte1)
				byte2 = file2.read(1)
				file0.write(byte2)
				if len(preview) < 512:
					preview += byte1.decode("cp437")
					preview += byte2.decode("cp437")
			file0.close()
			file1.close()
			file2.close()
			f1 = discord.File(input_path(ctx.message.id) + '/OUT.bin')
			await botMessage.add_files(f1)
			await sendMsgGuild(ctx, botMessage, preview + "```")
			status = 2
		else:
			await ctx.reply("Incorrect number of files attached", mention_author=False)
	else:
		botMessage = await ctx.reply("Error getting files", mention_author=False)

async def hash_file(filename):
	""""This function returns the SHA-1 hash
	of the file passed into it"""
	h = hashlib.sha1()
	with open(filename,'rb') as file:
		chunk = 0
		while chunk != b'':
			chunk = file.read(1024)
			h.update(chunk)
	return h.hexdigest()
