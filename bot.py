from decouple import config
import discord
from discord import AllowedMentions
from discord.ext import commands
import botext



description = '''Discord bot used to process BIOS files.'''
intents = discord.Intents.default()
intents.members = True
intents.message_content = True
bot_version = "1.7"

bot = commands.Bot(
    command_prefix="!",
    intents=intents,
    allowed_mentions=AllowedMentions(
        users=False,         # Whether to ping individual user @mentions
        everyone=False,      # Whether to ping @everyone or @here mentions
        roles=False,         # Whether to ping role @mentions
        replied_user=False,  # Whether to ping on replies to messages
    ),
)

# bot init message
@bot.event
async def on_ready():
    if bot.user != None:
        print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    else:
        print("Missing user?! Probably going boom")
    print('----------------------------')

# !ping
@bot.command()
async def ping(ctx: commands.Context):
    """Checks if the bot is alive"""
    await ctx.reply("Yo, I'm here!", mention_author=False)
# !ver
@bot.command()
async def ver(ctx: commands.Context):
    """Shows the bot version"""
    await ctx.reply("TRW BIOS analyzer bot, version " + bot_version, mention_author=False)

# !exist
@bot.command()
async def exist(ctx: commands.Context):
    """Checks if one or more BIOS files are present on TRW"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.checkTRW(ctx, botMessage)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")

# !b
@bot.command()
async def b(ctx: commands.Context):
    """Analyzes BIOS files and outputs results as text or spreadsheet"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, True, botMessage, True)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")
# !bios
@bot.command()
async def bios(ctx: commands.Context):
    """same command as !b"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, True, botMessage, True)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")
# !csv
@bot.command()
async def csv(ctx: commands.Context):
    """Analyzes BIOS files and outputs results as CSV spreadsheet"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, True, botMessage, False)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")
# !bns
@bot.command()
async def bns(ctx: commands.Context):
    """Analyzes BIOS files and outputs results only as text"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, False, botMessage, False)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")

# !split
@bot.command()
async def split(ctx: commands.Context):
    """Splits a BIOS file into ODD/EVEN"""
    if ctx.message.attachments:
        await botext.folderMaintenance(ctx.message.id)
        await botext.splitFiles(ctx)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach files, try again.")

# !join
@bot.command()
async def join(ctx: commands.Context):
    """Joins interleaved BIOS files into one. Upload order: ODD[HI] -> EVEN[LO]"""
    if ctx.message.attachments:
        await botext.folderMaintenance(ctx.message.id)
        await botext.joinFiles(ctx)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach files, try again.")

# !string
@bot.command()
async def string(ctx: commands.Context):
    """Analyzes the POST string for one or more BIOS files"""
    botMessage = await ctx.reply("Analyzing POST strings (given as text)")
    await botext.guessString(ctx, botMessage)
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.send("Fetching files")
        await botext.getAttachments(ctx)
        await botext.checkString(ctx, botMessage)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("```No attachments to check, skipping.```")
# !cookie
@bot.command()
async def cookie(ctx: commands.Context):
    botMessage = await ctx.reply(":cookie:")
# !epa
@bot.command()
async def epa(ctx: commands.Context):
    """Extracts BIOS logo from a given file"""
    if ctx.message.attachments or 'https://' in ctx.message.content or 'http://' in ctx.message.content:
        await botext.folderMaintenance(ctx.message.id)
        botMessage = await ctx.reply("Fetching files")
        await botext.getAttachments(ctx)
        await botext.processEpa(ctx, True, botMessage)
        await botext.folderCleanup(ctx.message.id)
    else:
        await ctx.send("You forgot to attach/link files, try again.")

bot.run(config('token',default=''))
