TRW BIOS Tools (86Box fork)
================
A toolkit for extracting and analyzing x86 BIOS ROM images.


### System requirements

* **Linux**
* **Python 3.5** or newer.
* **7-Zip** command line utility installed as `7z`.
* **QEMU** (`qemu-system-i386`) for optionally extracting files which need to be executed.

### Usage

All you have to do is to run `make`, then install all the python libraries in `requirements.txt` and run:

```
python3 bot.py
```

## Extraction notes

* Many common file types known not to be useful, such as images, PDFs, Office documents and hardware information tool reports, are automatically discarded.
* Interleaved ROMs are merged through a heuristic filename and string detection, which may lead to incorrect merging if the chunks to different interleaved ROMs are present in the same directory.
* The FAT filesystem extractor relies on assumptions which may not hold true for all disk images.
* EPA (Award), PCX (AMI), PGX (Phoenix) and other image formats are automatically converted to PNG if the aforementioned optional dependency is installed.
* Extraction of the following BIOS distribution formats is **not implemented** due to the use of unknown compression methods:
  * ICL `.LDB`

## Analysis notes

### AMI

* The string on **UEFI** is a hidden string located within the AMIBIOS 8-based Compatibility Support Module (CSM). A missing string may indicate a missing CSM.
* Metadata tag **Setup** indicates the setup type for AMIBIOS Color through 7: **Color**, **Easy**, **HiFlex**, **Intel**, **New**, **Simple** or **WinBIOS**.

### Award

* OEM modifications which may interfere with detection: **Sukjung** (string)
* Metadata tag **PhoenixNet** indicates the presence of PhoenixNet features, even if those were disabled by the OEM, and contains its splash screen's sign-on text.
* Metadata tag **UEFI** indicates Gigabyte Hybrid EFI.

### IBM

* The FRU codes contained in PC or PS/2 ROMs are interpreted as the string.

### Phoenix

* Some OEMs have modified Phoenix to a point where detection may not be perfect.

### SystemSoft

* Insyde-compressed modules (identified by magic bytes `FF 88`) cannot be decompressed, limiting the analyzer's ability to identify Insyde-branded SystemSoft BIOSes.

## Metadata reference

Depending on the contents of each BIOS, the following tags may be displayed on the analyzer output's "Metadata" column:

* **ACPI**: Appears to contain the ACPI tables specified. Does not necessarily indicate ACPI actually works.
* **Build**: Build information contained within the BIOS.
* **ID**: How the BIOS identifies itself during POST.
* **LAN**: PXE or Novell NetWare RPL-compliant network boot ROM, usually associated with on-board Ethernet.
* **SCSI**: Adaptec or NCR/Symbios SCSI option ROM. Model (Adaptec) or SDMS version (NCR/Symbios) information is extracted from the ROM.
* **SLI**: NVIDIA SLI license for non-nForce motherboards. Model information is extracted from the license header.
* **Table**: Register table information contained within the BIOS. May help in identifying chipset and Super I/O devices.
* **UEFI**: Appears to contain traces of UEFI. Does not necessarily indicate UEFI support is available.
* **VGA**: Non-PCI video BIOS, usually associated with on-board video.
